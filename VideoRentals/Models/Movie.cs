﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace VideoRentals.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Release Date")]
        public string ReleasedDate { get; set; }
        public string DateAdded { get; set; }
        [Display(Name = "Number in Stock")]
        [Range(1,20)]
        public int Stock { get; set; }
        public Genre Genre { get; set; }
        [Display(Name="Genre")]
        public int Genreid { get; set; }

        public byte NumberAvailable { get; set; }
    }
}