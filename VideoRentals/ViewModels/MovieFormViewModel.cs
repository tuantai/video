﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VideoRentals.Models;
using System.ComponentModel.DataAnnotations;

namespace VideoRentals.ViewModels
{
    public class MovieFormViewModel
    {
        public IEnumerable<Genre> Genres { get; set; }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Display(Name = "Release Date")]
        [Required]
        public string ReleasedDate { get; set; }
        
        [Display(Name = "Number in Stock")]
        [Required]
        [Range(1, 20)]
        public int Stock { get; set; }
        
        [Display(Name = "Genre")]
        [Required]
        public int Genreid { get; set; }

        public string Title
        {
            get
            {
                if (Id != 0)
                    return "Edit Movie";
                return "New Movie";
            }
        }

        public MovieFormViewModel()
        {
            Id = 0;
        }

        public MovieFormViewModel(Movie movie)
        {
            Id = movie.Id;
            Name = movie.Name;
            ReleasedDate = movie.ReleasedDate;
            Stock = movie.Stock;
            Genreid = movie.Genreid;
        }
    }
}