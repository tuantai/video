﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VideoRentals.Models;
using VideoRentals.ViewModels;
using System.Data.Entity;

namespace VideoRentals.Controllers
{
    public class MovieController : Controller
    {
            private ApplicationDbContext _context;      
            // GET: Movie

            public MovieController()
            {
                _context = new ApplicationDbContext();
            }

            protected override void Dispose(bool disposing)
            {
                _context.Dispose();
            }

        public ActionResult Random()
            {
                var movie = new Movie() { Name = "Robocop" };
                var customers = new List<Customer>()
                {
                    new Customer() {Name = "Customer 1"},
                    new Customer() {Name = "Customer 2"}
                };

                var viewModel = new RandomMovieViewModel()
                {
                    Movie = movie,
                    Customers = customers
                };

                return View(viewModel);
                //return Content("Hello world");
                //return HttpNotFound();
                //return RedirectToAction("Index", "Home", new {page = 1, sortBy = "name"});
            }

            //public ActionResult Edit(int id)
            //{
            //    return Content("id=" + id);
            //}

            public ViewResult Index()
            {
                //var movies = _context.Movies.Include(m => m.Genre).ToList();
                if (User.IsInRole(RoleName.ManageMovie))
                    return View("Index");
                return View("ReadOnlyList");
            }

            public ActionResult Details(int id)
            {
                var movie = _context.Movies.Include(m => m.Genre).SingleOrDefault(m => m.Id == id);

                if (movie == null)
                    return HttpNotFound();

                return View(movie);
            }

            /*
            public ActionResult Index(int? pageIndex, String sortBy)
            {
                if (!pageIndex.HasValue)
                    pageIndex = 1;
    
                if (String.IsNullOrWhiteSpace(sortBy))
                    sortBy = "Name";
    
                return Content(String.Format("pageIndex={0}&sortBy={1}", pageIndex, sortBy));
            }
            */

            [Route("movies/released/{year}/{month:range(1,12)}")]
            public ActionResult ByReleaseDate(int year, int month)
            {
                return Content(year + "/" + month);
            }

            [Authorize(Roles = RoleName.ManageMovie)]
            public ActionResult New()
            {
                var genres = _context.Genres.ToList();
                var viewmodel = new MovieFormViewModel
                {
                    Genres = genres
                };

                return View("MovieForm", viewmodel);
            }

            public ActionResult Edit(int id)
            {
                var movie = _context.Movies.SingleOrDefault(m => m.Id == id);

                if (movie == null)
                    return HttpNotFound();

                var viewModel = new MovieFormViewModel(movie)
                {
                    
                    Genres = _context.Genres.ToList()
                };

                return View("MovieForm", viewModel);
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Save(Movie movie)
            {
                if (!ModelState.IsValid)
                {
                    var viewModel = new MovieFormViewModel(movie)
                    {
                        
                        Genres = _context.Genres.ToList()
                    };

                    return View("MovieForm", viewModel);
                }

                if (movie.Id == 0)
                {
                    movie.DateAdded = DateTime.Now.ToString();
                    _context.Movies.Add(movie);
                }
                else
                {
                    var movieInDb = _context.Movies.Single(m => m.Id == movie.Id);
                    movieInDb.Name = movie.Name;
                    movieInDb.ReleasedDate = movie.ReleasedDate;
                    movieInDb.Genreid = movie.Genreid;
                    movieInDb.Stock = movie.Stock;
                }
                
                _context.SaveChanges();
                return RedirectToAction("Index", "Movie");
            }

        //private IEnumerable<Movie> GetMovies()
        //{
        //    return new List<Movie>
        //    {
        //        new Movie { Id = 1, Name = "Shrek" },
        //        new Movie { Id = 2, Name = "Wall-e" }
        //    };
        //}
    }
}