﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VideoRentals.Dtos
{
    public class MovieDto
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string ReleasedDate { get; set; }

        public string DateAdded { get; set; }

        public GenreDto Genre { get; set; }

        [Range(1, 20)]
        public int Stock { get; set; }
        
        public int Genreid { get; set; }
    }
}