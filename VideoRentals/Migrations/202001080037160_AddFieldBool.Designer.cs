// <auto-generated />
namespace VideoRentals.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddFieldBool : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddFieldBool));
        
        string IMigrationMetadata.Id
        {
            get { return "202001080037160_AddFieldBool"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
