namespace VideoRentals.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'225a116f-08d6-4950-8718-92ad42f96aab', N'test@test.com', 0, N'AEIUIpBiWhsDtkspBX6wKqk0wX9n+1Klr1YtGVpiYxdBIf8hf3XaMX20QlSz+FN4yQ==', N'1f49f6b2-9bff-4640-88b2-adf67b96d6ba', NULL, 0, 0, NULL, 1, 0, N'test@test.com')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'62c1726d-3061-418f-870d-ca9355ab33b4', N'test2@test.com', 0, N'APP/5PX+kUGoeF/pQX2hWMb2A6elPpuHZ+KDICBkhVXqbNOZIpkyPW02G6YLhBPvYA==', N'f59e45fd-6087-47f0-af10-1b79231036cc', NULL, 0, 0, NULL, 1, 0, N'test2@test.com')

                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'b4944bd3-3ba4-4e11-87df-914241c7fee9', N'ManageMovie')

                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'62c1726d-3061-418f-870d-ca9355ab33b4', N'b4944bd3-3ba4-4e11-87df-914241c7fee9')
            ");
        }
        
        public override void Down()
        {
        }
    }
}
